<?php
/**
 * The template for displaying all services
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package StrapPress
 */

get_header(); ?>


<div id="col-primary" class="col-content-area">
	<main id="col-main" class="col-site-main" role="main">

		<?php query_posts( array(
		   'post_type' => 'testimonial',
		   'posts_per_page' => 3,
		)); ?>
					<?php $counter = 0;
					while ( have_posts() ) : the_post();  $counter++;

					   // echo $current_id;
						 ?>

<div class="row-wrp">
 <div class="parent-col <?php if( $counter % 2 == 0 ): echo"left"; else: echo"right";  endif;?>">
	 <div class="child-col <?php if( $counter % 2 == 0 ): echo"right"; else: echo"left";  endif;?>">
		 <h2><?php the_title(); ?></h2>
		 <h3><?php the_field('services'); ?></h3>
		 <div class="hm-row-txt">
			 <?php the_excerpt(); ?>
			 <a href="<?php  the_permalink();?>" class="btn btn-outline-primary">Read More</a>
		 </div>
	 </div>
 </div>
 <div class="parent-col <?php if( $counter % 2 == 0 ): echo"right"; else: echo"left";  endif;?>">
		 <img src="<?php the_field('hero_bg');?>" class="col-img" alt=""/>
 </div>
</div>

<?php
					endwhile; // End of the loop.
					?>
	<?php wp_reset_postdata(); ?>
<!-- end top loop reset time -->


<?php query_posts( array(
	 'post_type' => 'testimonial',
	 'posts_per_page' => 6,
	 'offset' => 3
)); ?>
			<?php if (have_posts()) :?>
				<div class="row-wrp">
				<div class="parent-col left">
				<div class="child-col right">
				 <h2 class="testimonial_section_title">More Testimonials</h2>
			 </div>
		 </div>
	 </div>
	 <div class="row-wrp m0015 ">
			<?while ( have_posts() ) : the_post();
				 // echo $current_id;
				 ?>
<div class="testimoanil-more-wrp">
	<a href="<?php the_permalink();?>" class="more-testimonial-thumb" style="background-image:url('<?php the_field('hero_bg');?>');">

 	</a>
</div>


<?php
			endwhile; // End of the loop.
		endif;
			?>
			</div>
<?php wp_reset_postdata(); ?>

	</main>
</div>


<?php
get_footer();

<script>
jQuery(document).ready(function( $ ) {

	$('.carousel').carousel({
  interval:false,
  pause: true
});


});
</script>
<div id="page-hero" class="hero" style="background-image:url('<?php the_field('home_hero_background');?>');">
	<div class="inner">
	 <div class="copy vertical">
	 <h1><?php the_field('hero_heading')?></h1>
	 </div>
	 <div class="testimonials-slider vertical">
		<div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner" role="listbox">
    <div class="carousel-item active">
			<div class="item-row">
      <p>"Jordan Larson and his crew did an amazing job on a huge project for a duplex. Jordan was super easy to deal from the quoting process to the final installation..." </p>
			</div>
			<div class="item-row">
			<a href="#" class="btn btn-outline-primary">Read More</a>
			</div>
		</div>
    <div class="carousel-item">
			<div class="item-row">
      <p>"Jordan Larson and his crew did an amazing job on a huge project for a duplex. Jordan was super easy to deal from the quoting process to the final installation..." </p>
			</div>
			<div class="item-row">
			<a href="#" class="btn btn-outline-primary">Read More</a>
			</div>
    </div>
    <div class="carousel-item">
			<div class="item-row">
      <p>"Jordan Larson and his crew did an amazing job on a huge project for a duplex. Jordan was super easy to deal from the quoting process to the final installation..." </p>
			</div>
			<div class="item-row">
			<a href="#" class="btn btn-outline-primary">Read More</a>
			</div>
    </div>
  </div>

</div>
	</div>
 </div>
</div>

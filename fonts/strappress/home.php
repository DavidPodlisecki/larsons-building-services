<?php
/**
 *Template Name: Homepage
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package StrapPress
 */

get_header(); ?>


			<div id="col-primary" class="col-content-area">
				<main id="col-main" class="col-site-main" role="main">
					<?php

					while ( have_posts() ) : the_post(); ?>
					<!--
						// get_template_part( 'template-parts/content', 'page' );
						//
						// // If comments are open or we have at least one comment, load up the comment template.
						// if ( comments_open() || get_comments_number() ) :
						// 	comments_template();
						// endif; -->

						<?php

						// check if the flexible content field has rows of data
						if( have_rows('content_sections') ):

						     // loop through the rows of data
						    while ( have_rows('content_sections') ) : the_row();

						        if( get_row_layout() == 'services' ):
											?>


											<div class="row-wrp">
												<div class="parent-col left">
													<div class="child-col right">
														<h2><?php the_sub_field('headline'); ?></h2>
														<h3><?php the_sub_field('sub_head'); ?></h3>
														<div class="hm-row-txt">
															<?php the_sub_field('text'); ?>
															<a href="services/" class="btn btn-outline-primary">Learn More</a>
														</div>
													</div>
												</div>
												<div class="parent-col right">
													<div class="child-col left">
														<a class="servce-btn" href="services/siding">
															<span class="icon-siding"></span>
															Siding
														</a>
														<a class="servce-btn" href="services/roofing">
															<span class="icon-roofing"></span>
															Roofing
														</a>
														<a class="servce-btn" href="services/windows">
															<span class="icon-windows"></span>
															Windows
														</a>
														<a class="servce-btn" href="services/gutters">
															<span class="icon-gutters"></span>
															Gutters
														</a>
														<a class="servce-btn" href="services/doors">
															<span class="icon-door"></span>
															Doors
														</a>
														<a class="servce-btn" href="services">
															<span class="icon-all"></span>
															All
														</a>

													</div>
												</div>
											</div>
											<?php


									elseif( get_row_layout() == 'vendor_callout' ):

											?>


 										 <div class="row-wrp">
 											 <div class="parent-col right">
 												 <div class="child-col left">
 													 <h2><?php the_sub_field('headline'); ?></h2>
 													 <h3><?php the_sub_field('sub_head'); ?></h3>
													 <div class="hm-row-txt">
														 <?php the_sub_field('text'); ?>
														 <a href="services/siding" class="btn btn-outline-primary">Learn More</a>
													 </div>
 												 </div>
 											 </div>
 											 <div class="parent-col left">
 													 <img src="<?php the_sub_field('section_image');?>" class="col-img" alt="">
 											 </div>
 										 </div>
 										 <?php

										 elseif( get_row_layout() == 'contact_cta' ):

	 										?>


	 									 <div class="row-wrp img-row" style=" background-image: url(<?php the_sub_field('section_image');?>);">
	 										 <div class="parent-col left">
	 											 <div class="child-col right">
	 												 <h2><?php the_sub_field('headline'); ?></h2>
	 												 <h3><?php the_sub_field('sub_head'); ?></h3>
	 												 <div class="hm-row-txt">
	 													 <?php the_sub_field('text'); ?>
	 													 <a href="contact" class="btn btn-outline-primary">Learn More</a>
	 												 </div>
	 											 </div>
	 										 </div>
	 										 <div class="parent-col right">
	 												 <img src="<?php the_sub_field('section_map_image');?>" class="col-img" alt="">
	 										 </div>
	 									 </div>
	 									 <?php


									elseif( get_row_layout() == 'About' ):

										 ?>


										 <div class="row-wrp">
											 <div class="parent-col right">
												 <div class="child-col left">
													 <h2><?php the_sub_field('headline'); ?></h2>
													 <h3><?php the_sub_field('sub_head'); ?></h3>
													 <div class="hm-row-txt">
														 <?php the_sub_field('text'); ?>
														 <a href="about" class="btn btn-outline-primary">Learn More</a>
													 </div>
												 </div>
											 </div>
											 <div class="parent-col left">
													 <img src="<?php the_sub_field('section_image');?>" class="col-img" alt="">
											 </div>
										 </div>

										 <?php

									 endif;


						    endwhile;

						else :

						    // no layouts found

						endif;

						?>

						<?php
					endwhile; // End of the loop.
					?>

				</main><!-- #main -->
			</div><!-- #primary -->




	</div><!-- #page -->
<?php

get_footer();

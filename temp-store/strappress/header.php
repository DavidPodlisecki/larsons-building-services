<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package StrapPress
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
<script src="https://use.typekit.net/hyn1dle.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>
</head>

<body <?php body_class(); ?>>
	<div id="wptime-plugin-preloader"></div>
<div id="page" class="site">

	<header id="masthead" class="site-header" role="banner">
	    <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
	    	<div class="container-fluid">

				<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				</button>
				 <div class="navbar-brand mb-0"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php bloginfo('template_url')?>/images/Larsons_building_services_logo.png"/></a></div>
		   		<div class="collapse navbar-collapse" id="navbarNav">
	            <?php
	            $args = array(
	              'theme_location' => 'primary',
	              'depth'      => 2,
	              'container'  => false,
	              'menu_class'     => 'navbar-nav',
	              'walker'     => new Bootstrap_Walker_Nav_Menu()
	              );
	            if (has_nav_menu('primary')) {
	              wp_nav_menu($args);
	            }
	            ?>
	          </div>
						<div id="header-contact">
						<div class="phone">
							<i class="fa fa-phone-square" aria-hidden="true"></i> 123.132.1234
						</div>

							<a href="#" class="btn btn-outline-primary" data-toggle="modal" data-target="#modal-transparent">Free sQuote</a>
							<!-- Modal transparent -->
							<div class="modal modal-transparent fade" id="modal-transparent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  <div class="modal-dialog">
							    <div class="modal-content">
							      <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
							      </div>
							      <div class="modal-body">
							        ...
							      </div>
							      <div class="modal-footer">
							        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							      </div>
							    </div>
							  </div>
							</div>

	        </div>

		</nav>
	</header><!-- #masthead -->
	<div id="content" class="site-content">
		<!-- PAGE HERO -->
		<?php
		if (is_post_type_archive('testimonial')) {
			get_template_part('template-parts/hero', 'testimonial');
		} else {
			while ( have_posts() ) : the_post();
					if (is_page_template('home.php')) {
					// Default homepage
					get_template_part( 'template-parts/hero', 'home' );
				} elseif (is_page_template('services.php')) {
					get_template_part('template-parts/hero', 'services');
				} elseif (is_page_template('contact.php')) {
					get_template_part('template-parts/hero', 'contact');
				} elseif(is_singular( 'testimonial' ) ){

				get_template_part( 'template-parts/hero', 'page' );

			 }
				 else{
					//everything else
					get_template_part( 'template-parts/hero', 'page' );
				};
			endwhile; // End of the loop.
		};
		?>
		<?php wp_reset_postdata(); ?>



<!-- END PAGE HERO -->

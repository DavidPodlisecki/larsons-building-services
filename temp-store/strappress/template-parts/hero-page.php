<div id="page-hero" class="hero page-hero" style="background-image:url('<?php the_field('page_hero_background');?>');">
	<div class="inner">
	 <div class="copy vertical">
	 <h1>
		<?php if ( get_field( 'alt_title' ) ): ?>

			 <?php the_field('alt_title');?>
<?php else: // field_name returned false ?>

<?php the_title();?>
<?php endif; // end of if field_name logic ?>
		</h1>
	 </div>

 </div>
</div>

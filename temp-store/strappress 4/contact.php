<?php
/** Template Name: Contact
 * The template for displaying all services
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package StrapPress
 */

get_header(); ?>

<div class="row-wrp">
	<?php
	while ( have_posts() ) : the_post();


	?>
	<div class="row-wrp m0a">
	 <div class="m1200">
		 <style media="screen">

		 </style>

		 <?php echo do_shortcode('[contact-form-7 id="67" title="Contact form 1"]'); ?>

	 </div>
	</div>
	<!-- <script>
	jQuery(document).ready( function($){

			$("#add-photo").click(function(){
					$(".add_list_item").first().click();
					return false;
			});
	});
	</script> -->


<?php



	endwhile; // End of the loop.
	?>

</div>




<?php
get_footer();

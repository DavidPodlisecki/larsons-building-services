<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package StrapPress
 */

?>

<div class="row-wrp logo-bar">

	<?php if( have_rows('logos', 'options') ): ?>

	    <ul id="footer-logos">

	    <?php while( have_rows('logos', 'options') ): the_row(); ?>

	        <li> <img src="<?php the_sub_field('logo', 'options');?>"/></li>

	    <?php endwhile; ?>

	    </ul>

	<?php endif; ?>

</div>
<div class="row-wrp footer-mid">

	<div class="ft-tag-wrp"><?php the_field('tagline', 'options'); ?></div>
	<div class="ft-soc-wrp">
	<?php the_field('social_text', 'options'); ?>
<i class="fa fa-twitter" aria-hidden="true"></i>  <i class="fa fa-facebook-official" aria-hidden="true"></i> <i class="fa fa-instagram" aria-hidden="true"></i>



	<!-- <?php the_field('social_media', 'options'); ?> -->
	</div>
	<div class="ft-cta-wrp">
	<?php the_field('cta', 'options'); ?>
	</div>
</div>
<div class="row-wrp footer-lwr">

		<div class="container-fluid">

		 <div class="navbar-brand mb-0"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php bloginfo('template_url')?>/images/footer-logo.png"/></a></div>
			<div class="footer-nav" id="navbarNav">
					<?php
					$args = array(
						'theme_location' => 'primary',
						'depth'      => 2,
						'container'  => false,
						'menu_class'     => 'footer-navbar-nav',
						'walker'     => new Bootstrap_Walker_Nav_Menu()
						);
					if (has_nav_menu('primary')) {
						wp_nav_menu($args);
					}
					?>
				</div>
				<div id="footer-contact">
				<div class="phone">
					<i class="fa fa-phone-square" aria-hidden="true"></i> 123.132.1234
				</div>

					<a href="#" class="btn btn-outline-primary" data-toggle="modal" data-target="#modal-fullscreen">Free Quote</a>

			</div>


</div>
<script>
jQuery(document).ready( function($){

	//$(".add_list_item:contains('Browse')").html("+");
	$(document).on("click", '.cf7rcwdupload-pickfiles', function() {

		$(this).closest(".cf7rcwdupload-container").find(".add_list_item").click();
		console.log("test")
		//$(".add_list_item").first().click();
		 event.preventDefault();
		return false;
	 });

	// $(".cf7rcwdupload-pickfiles").click(function(e){
	//
	// 	console.log("fst s test");
	// 		$(this).closest(".cf7rcwdupload-container").find(".add_list_item").click();
	// 		console.log("test")
	// 	  //$(".add_list_item").first().click();
	// 		 event.preventDefault();
	// 		return false;
	// });
$(".modal-fullscreen").on('show.bs.modal', function () {
setTimeout( function() {
	$(".modal-backdrop").addClass("modal-backdrop-fullscreen");
}, 0);
});
$(".modal-fullscreen").on('hidden.bs.modal', function () {
$(".modal-backdrop").addClass("modal-backdrop-fullscreen");
});

});
</script>
<?php wp_footer(); ?>

</body>
</html>

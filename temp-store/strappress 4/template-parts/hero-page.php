<div id="page-hero" class="hero page-hero" style="background-image:url('<?php the_field('hero_bg');?>');">
	<div class="inner">
	 <div class="copy vertical">

		<h1>
			<?php if(is_singular( 'testimonial' ) ){
				?>
		 "<?php the_field('pull_quote'); ?>"
		 <br/>
		 —<?php the_field('client');?>
<?
}
else{
     if ( get_field( 'alt_title' ) ){
			 the_field('alt_title');
		 }else{
			  the_title();
		 };
} ?>
	 </div>

 </div>
</div>

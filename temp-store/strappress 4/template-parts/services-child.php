<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package StrapPress
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


	<div class="row-wrp m0a">
 	 <div class="parent-col left">
 		 <div class="child-col right">
 			 <div class="hm-row-txt">
			 	<h2><?php the_title(); ?></h2>
	 			<h3><?php the_field('sub_head'); ?></h3>
				 <?php
		 			the_content();
		 		?>
				 <span class="service_page_cta">Get your free custom windows quote today.</span>
 				 <a href="#" class="btn btn-outline-primary">Free Quote</a>
 			 </div>
 		 </div>
 	 </div>
 	 <div class="parent-col right">
 			 <img src="<?php the_field('section_image1');?>" class="col-img" alt="">
			 <img src="<?php the_field('section_image2');?>" class="col-img2" alt="">
 	 </div>
  </div>
 <div class="row-wrp m0a">
	 <!-- <img src="<?php the_field('section_image3');?>" class="col-img3" alt=""> -->

<div class="row-wrp pg-img-row" style=" background-image: url(<?php the_field('section_image3');?>);">

</div>
		<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
				edit_post_link(
					sprintf(
						/* translators: %s: Name of current post */
						esc_html__( 'Edit %s', 'strappress' ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					),
					'<span class="edit-link">',
					'</span>'
				);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-## -->

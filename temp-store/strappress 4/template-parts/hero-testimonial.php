<div id="page-hero" class="hero page-hero" style="background-image:url('<?php the_field('hero_bg', 10);?>');">
	<div class="inner">
	 <div class="copy vertical">
	 <h1>
		<?php if ( get_field( 'alt_title', 10 ) ): ?>

			 <?php the_field('alt_title', 10);?>
<?php else: // field_name returned false ?>

<?php the_title('10');?>
<?php endif; // end of if field_name logic ?>
		</h1>
	 </div>

 </div>
</div>

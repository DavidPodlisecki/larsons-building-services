<?php
/**
 * The template for displaying all services
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package StrapPress
 */

get_header(); ?>


<div id="col-primary" class="col-content-area">
	<main id="col-main" class="col-site-main" role="main">

					<?php
					while ( have_posts() ) : the_post();

?>
<div class="row-wrp ">
	<div class="parent-col m1200">
		<div class="child-col left">
			<h2><?php the_title(); ?></h2>
			<h3><?php the_field('services'); ?></h3>
		</div>
	</div>
</div>
<div class="split-wrp">
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content split-col">

		<?php
			the_content();
		?>
	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
				edit_post_link(
					sprintf(
						/* translators: %s: Name of current post */
						esc_html__( 'Edit %s', 'strappress' ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					),
					'<span class="edit-link">',
					'</span>'
				);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-## -->
</div>

<?php
					endwhile; // End of the loop.
					?>

	</main>
</div>


<?php
get_footer();

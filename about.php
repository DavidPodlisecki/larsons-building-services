<?php
/** Template Name: About
 * The template for displaying all services
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package StrapPress
 */

get_header(); ?>


<div id="col-primary" class="col-content-area">
	<main id="col-main" class="col-site-main" role="main">

					<?php
					while ( have_posts() ) : the_post();

?>
<?php

// check if the flexible content field has rows of data
if( have_rows('content_sections') ):

		 // loop through the rows of data
		while ( have_rows('content_sections') ) : the_row();

				if( get_row_layout() == 'team_about' ):
					?>
					<div class="row-wrp">
						<div class="parent-col m1200">
							<div class="child-col left">
								<h2><?php the_sub_field('heading');?></h2>
								<h3><?php the_sub_field('sub_heading');?></h3>
							</div>
						</div>
					</div>
					<div class="split-wrp">

						<div class="entry-content split-col">

							<?php the_sub_field('text_area');?>
						</div><!-- .entry-content -->
					</div>

					<?php
					elseif( get_row_layout() == 'team_section' ):

							?>

							<div class="split-wrp">
								<?php if( have_rows('team_members') ): ?>

									    <ul class="team-list">

									    <?php while( have_rows('team_members') ): the_row(); ?>

									        <li>
														<div class="member-pic" style='background-image: url(<?php the_sub_field('photo'); ?>)'></div>
														<span class="name"><?php the_sub_field('name'); ?></span>
														<span class="title"><?php the_sub_field('title'); ?></span>
													</li>



									    <?php endwhile; ?>

									    </ul>

									<?php endif; ?>
							</div>

			<?php elseif( get_row_layout() == 'about_section' ):

					?>

					<div class="row-wrp">
				 	 <div class="parent-col left">
				 		 <div class="child-col right">
				 			 <div class="hm-row-txt">
							 	<h2><?php the_sub_field('heading');?></h2>
					 			<h3><?php the_sub_field('sub_heading');?></h3>
								 <?php the_sub_field('text_area');?>

				 			 </div>
				 		 </div>
				 	 </div>
				 	 <div class="parent-col right">
				 			 <img src="<?php the_sub_field('side_column_image_1');?>" class="col-img" alt="">
							 <img src="<?php the_sub_field('side_column_image_2');?>" class="col-img2" alt="">
				 	 </div>
				 </div>
					<div class="row-wrp pg-img-row parallax-window"  iosFix="false"  data-parallax="scroll"  data-image-src="<?php the_sub_field('lower_image');?>"></div>

				<?php endif;?>
			<?php endwhile;?>
		<?php endif ?>

<?php
					endwhile; // End of the loop.
					?>

	</main>
</div>


<?php
get_footer();

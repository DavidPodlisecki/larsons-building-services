<?php
/**
 * Enqueue scripts and styles.
 */
function strappress_scripts() {
	wp_enqueue_style( 'strappress-style', get_stylesheet_directory_uri() . '/style.min.css', array(), '1.0.0' );

	wp_enqueue_script( 'strappress-js', get_template_directory_uri() . '/js/dist/scripts.min.js', array('jquery'), ' ', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'strappress_scripts' );


// 
// function wpb_adding_scripts() {
// 	wp_enqueue_style( 'animate', get_template_directory_uri() . '/bower_components/wow/css/libs/teststyle.css' );
//   wp_register_script('animate', get_template_directory_uri() . '/bower_components/wow/dist/wow.js', array(),'', true);
// wp_enqueue_script('animate');
// }
//
// add_action( 'wp_enqueue_scripts', 'wpb_adding_scripts' );

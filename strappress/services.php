<?php
/** Template Name: Services
 * The template for displaying all services
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package StrapPress
 */

get_header(); ?>


<div id="col-primary" class="col-content-area">
	<main id="col-main" class="col-site-main" role="main">

					<?php
					while ( have_posts() ) : the_post();


					if( is_page() && $post->post_parent > 1 ) {

					get_template_part( 'template-parts/services', 'child' );

						} else {

							get_template_part( 'template-parts/services', 'parent' );
						}


						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;

					endwhile; // End of the loop.
					?>

	</main>
</div>


<?php
get_footer();

<div id="page-hero" class="row-wrp hero img-row parallax-window" iosFix="false"  data-parallax="scroll"  data-image-src="<?php the_field('hero_bg');?>">
	<div class="parent-col left wow fadeInUp" data-wow-duration="2s">
		<div class="child-col right">
			<h1>
				<h1>
					<?php if(is_singular( 'testimonial' ) ){
						?>
				 "<?php the_field('pull_quote'); ?>"
				 <br/>
				 —<?php the_field('client');?>
		<?
		}
		else{
		     if ( get_field( 'alt_title' ) ){
					 the_field('alt_title');
				 }else{
					  the_title();
				 };
		} ?>
			</h1>
		</div>
	</div>


	</div>

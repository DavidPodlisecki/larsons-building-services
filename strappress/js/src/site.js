jQuery(document).ready(function($){

document.addEventListener( 'wpcf7mailsent', function( event ) {

    $(".form-step").removeClass("active-step");
    $("#quote-section-main-end").addClass("active-step");
     return false;
}, false);

new WOW().init();

$(function () {
  $('[data-toggle="popover"]').popover();
  var image = '<img src="/larsonsbuildingservices/wp-content/themes/Larsons_Theme/images/sample_img_1.jpg""> <img src="/larsonsbuildingservices/wp-content/themes/Larsons_Theme/images/sample_img_2.jpg""> <img src="/larsonsbuildingservices/wp-content/themes/Larsons_Theme/images/sample_img_3.jpg"">';
	$('.popover-no-btn').popover({placement: 'top', trigger: 'hover', content: image, html: true});
});

// 	$(function(){
// 	    console.log('dom ready');
// 	    setTimeout(function() {
// 	        if($(".cf7_rcwdupload_add_list_item").is(':visible')){
// 	            console.log('element is visible. do stuff.');
//
// 							$(this).click(function (){
// 								console.log('it clicked add');
// 								$(".cf7_rcwdupload_delete_list_item").first().click();
// 								$(this).closest(".cf7_rcwdupload_delete_list_item").click();
// 								$('.wpcf7-form-control-wrap').first().find(".cf7_rcwdupload_delete_list_item").trigger("click");
// 								console.log('it removed the 1st?');
// 							});
// 							//$(this).click();
//
//
// 	        }
// 	    }, 10);
// 	});
//
// console.log("test");
// $('input[type="file"]').each(function() {
//     // get label text
//     var label = $(this).parents('.form-group').find('label').text();
//     label = (label) ? label : 'Upload File';
//
//     // wrap the file input
//     $(this).wrap('<div class="input-file"></div>');
//     // display label
//     $(this).before('<span class="btn btn-outline-primary ">'+label+'</span>');
//     // we will display selected file here
//     $(this).before('<span class="file-selected"></span>');
//
//     // file input change listener
//     $(this).change(function(e){
//         // Get this file input value
//         var val = $(this).val();
//
//         // Let's only show filename.
//         // By default file input value is a fullpath, something like
//         // C:\fakepath\Nuriootpa1.jpg depending on your browser.
//         var filename = val.replace(/^.*[\\\/]/, '');
//
//         // Display the filename
//         $(this).siblings('.file-selected').text(filename);
//     });
// });
//
// // Open the file browser when our custom button is clicked.
// $('.input-file .btn').click(function() {
//     $(this).siblings('input[type="file"]').trigger('click');
// });



var isMobile = {
          Android: function () {
              return navigator.userAgent.match(/Android/i);
          },
          BlackBerry: function () {
              return navigator.userAgent.match(/BlackBerry/i);
          },
          iOS: function () {
              return navigator.userAgent.match(/iPhone|iPad|iPod/i);
          },
          Opera: function () {
              return navigator.userAgent.match(/Opera Mini/i);
          },
          Windows: function () {
              return navigator.userAgent.match(/IEMobile/i);
          },
          any: function () {
              return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
          }
      };

      if (isMobile.any()) {
      // make mobile stuff happen if you need.
      }
      else {
        $(".add_list_item:contains('Browse')").html("+");

        // $(document).on("click", '.cf7rcwdupload-pickfiles', function() {
        //
        //   $(this).closest(".cf7rcwdupload-container").find(".add_list_item").click();
        //   console.log("test");
        //   $(".add_list_item").first().click();
        //    event.preventDefault();
        //   return false;
        //  });
        //
        // $(".cf7rcwdupload-pickfiles").click(function(e){
        //
        //   $(this).closest(".cf7rcwdupload-container").find(".add_list_item").click();
        //
        //    // $(".add_list_item").first().click();
        //      event.preventDefault();
        //     return false;
        //  });
      }

      // $(".cf7rcwdupload-button").click(function(){
      // //  $(this).addClass("remove");
      // });
      // $(".cf7rcwdupload-container a").click(function(){
      //   console.log("FUck you fucking fuck");
      //   $(this).closest(".remove").remove();
      // });

      $('.cf7rcwdupload-button').addClass("icon-upload-img");


// The mmMMMMMmuilti-formualtron //
$(".main-modal").click(function(){
  var is_active = false;
  console.log("I CLICKED");
  $(".form-step").each(function() {
    if ($(this).hasClass("active-step")) {
      // Check to see if an active state is present
      is_active = true;
    }
  });
  if (!is_active) {
      // If by some weird error the dam thing glitches oot hopefully this keeps it togethe!!!!! //
      $("#quote-section-main-1").addClass("active-step");
  }
});

$("#next-project").click(function(){
  var project =  $("#projectfields").val();
  var errors = false;
  var emailReg = /^([a-zA-Z0-9\\.]+)@([a-zA-Z0-9\\-\\_\\.]+)\.([a-zA-Z0-9]+)$/i;

  //validate email
  $(".wpcf7-form-control").removeClass("highlight");
  $(".validate-warning").removeClass("its_on");
  //refresh error messages on submit
  if($("input[name=your-name]").val() === ""){
    $("input[name=your-name]").addClass("highlight");
    errors = true;
  }
  //validate name field has entry
  if($("input[name=your-email]").val() === ""){
    $("input[name=your-email]").addClass("highlight");
    if($(".validate-warning").hasClass("its_on")){
    } else {
      $(".validate-warning").addClass("its_on");
    }
    errors = true;
  }if(!emailReg.test($("input[name=your-email]").val())){
    $("input[name=your-email]").addClass("highlight");
    errors = true;
  }else if($("#projectfields").val() === "Project Type"){
    $("#quote-section-main-1 .dyn").addClass("highlight");
    $("#projectfields").after("<span class='errors'>Select a service to quote</span>");
    errors = true;
  }else {
    //if yeah then move to next step
    if (project == "Siding") {
      $(".form-step").removeClass("active-step");
      $("#quote-section-siding-2").addClass("active-step");
    } else if (project == "Roofing") {
      $(".form-step").removeClass("active-step");
      $("#quote-section-roofing-2").addClass("active-step");
    } else if (project == "Windows") {
      $(".form-step").removeClass("active-step");
      $("#quote-section-windows-2").addClass("active-step");
    } else if (project == "Gutters") {
      $(".form-step").removeClass("active-step");
      $("#quote-section-gutters-2").addClass("active-step");
    } else if (project == "Doors") {
      $(".form-step").removeClass("active-step");
      $("#quote-section-doors-2").addClass("active-step");
    }
  }
  // next and back start //
    var id;

  $(".next-button").click(function(){
    var id = $(this).closest("section").attr("id");
    var base_id = id.slice(0,-1);
    var last_val = id.split("-").pop(-1);
    var next = +last_val+1;
    var next_id = base_id + next;
    var isFormValid = true;
    var all_answered = true;
    $("#"+id+ " select").each(function(){
      if ($.trim($(this).val()).length === 0){
        $(this).closest(".sel").addClass("highlight");
          isFormValid = false;
        $(this).focus();
      } else {
        $(this).removeClass("highlight");
      }
    });
    $("#"+id+ " .wpcf7-exclusive-checkbox input:checkbox").each(function(){
        var name = $(this).attr("name");
        if($("input:checkbox[name="+name+"]:checked").length === 0){
            all_answered = false;
            $(this).closest(".radio-group").addClass("highlight");
        } else {
            $(this).closest(".radio-group").removeClass("highlight");
          }
    });
    if (!isFormValid || !all_answered ) {
        $(".validate-warning").addClass("its_on");
    } else {

      if($(".sel").hasClass("highlight")){
         $(".sel").removeClass("highlight");
      }
      if($(".validate-warning").hasClass("its_on")){
         $(".validate-warning").removeClass("its_on");
      }
      if($(".radio-group").hasClass("highlight")){
         $(".radio-group").removeClass("highlight");
      }

      $(".form-step").removeClass("active-step");
      $("#"+next_id).addClass("active-step");
      return isFormValid;
    }
  });
  $(".last-button").click(function(){
    var id = $(this).closest("section").attr("id");
    var base_id = id.slice(0,-1);
    var last_val = id.split("-").pop(-1);
    var next = +last_val+1;
    var next_id = base_id + next;
    var isFormValid = true;
    var all_answered = true;
    $("#"+id+ " select").each(function(){
      if ($.trim($(this).val()).length === 0){
        $(this).closest(".sel").addClass("highlight");
          isFormValid = false;
        $(this).focus();
      } else {
        $(this).removeClass("highlight");
      }
    });
    $("#"+id+ " .wpcf7-exclusive-checkbox input:checkbox").each(function(){
        var name = $(this).attr("name");
        if($("input:checkbox[name="+name+"]:checked").length === 0){
            all_answered = false;
            $(this).closest(".radio-group").addClass("highlight");
        } else {
            $(this).closest(".radio-group").removeClass("highlight");
          }
    });
    if (!isFormValid || !all_answered ) {
        $(".validate-warning").addClass("its_on");
    } else {

      if($(".sel").hasClass("highlight")){
         $(".sel").removeClass("highlight");
      }
      if($(".validate-warning").hasClass("its_on")){
         $(".validate-warning").removeClass("its_on");
      }
      if($(".radio-group").hasClass("highlight")){
         $(".radio-group").removeClass("highlight");
      }

      $(".form-step").removeClass("active-step");
      $("#quote-section-last").addClass("active-step");
      $('input[name=hiddeninputname]').val(id);
      return isFormValid;
    }




    });
    $(".end-back-button").click(function(){
      var id = $("input[name=hiddeninputname]").val();
      $(".form-step").removeClass("active-step");
      $("#"+id).addClass("active-step");
   });
  $(".back-button").click(function(){
   var id = $(this).closest("section").attr("id");
   var base_id = id.slice(0,-1);
   var last_val = id.split("-").pop(-1);
   var back = +last_val-1;
   var back_id = base_id + back;
     $(".form-step").removeClass("active-step");
     $("#"+back_id).addClass("active-step");
 });
 $(".back-start-button").click(function(){
     $(".form-step").removeClass("active-step");
     $("#quote-section-main-1").addClass("active-step");
 });

  //next and back end //
  return !errors;
});
// end da muilti-formualtron //

/* ===== Logic for creating fake Select Boxes ===== */


$('.sel').each(function() {

  $(this).children('select').css('display', 'none');

  var $current = $(this);

  $(this).find('option').each(function(i) {
    if (i === 0) {
      $current.prepend($('<div>', {
        class: $current.attr('class').replace(/sel/g, 'sel__box')
      }));



        var placeholder = $(this).text();

      $current.prepend($('<span>', {
        class: $current.attr('class').replace(/sel/g, 'sel__placeholder'),
        text: placeholder,
        'data-placeholder': placeholder
      }));
      console.log("somthing going on?");

      return;
    }
    $current.children('div').append($('<span>', {
      class: $current.attr('class').replace(/sel/g, 'sel__box__options'),
      text: $(this).text()
    }));




  });
});



// Toggling the `.active` state on the `.sel`.
$('.sel').click(function() {
  $(this).toggleClass('active');
});


// Toggling the `.selected` state on the options.
$('.sel__box__options').click(function() {
  var txt = $(this).text();
  var index = $(this).index();

  $(this).siblings('.sel__box__options').removeClass('selected');
  $(this).addClass('selected');

  var $currentSel = $(this).closest('.sel');
  $currentSel.children('.sel__placeholder').text(txt);
  $currentSel.children('select').prop('selectedIndex', index + 1);
});




$('.dyn').each(function() {
  var fieldval =  $("input.wpcf7-dynamichidden").val(); // set the "selected" attribute for option with value equal to variable
  var val = fieldval.replace(/[ ,]\s/g,''); // remove any extra funny characters
  console.log(val);
  $('select#projectfields option[value=' + val + ']').attr('selected', 'selected');
  $(this).children('select').css('display', 'none');

  var $current = $(this);

  $(this).find('option').each(function(i) {
    if (i === 0) {
      $current.prepend($('<div>', {
        class: $current.attr('class').replace(/dyn/g, 'dyn__box')
      }));
      var hiddenValue = $("input.wpcf7-dynamichidden").val();
      // if(hiddenValue === "Siding" || hiddenValue === "Roofing" || hiddenValue === "Windows" || hiddenValue === "Doors" || hiddenValue === "Gutters" ) {
      //    holderValue = hiddenValue;
      // }else {
        if ($.inArray(hiddenValue, ['Siding', 'Roofing', 'Windows', 'Gutters', 'Doors']) >= 0) {
          holderValue = hiddenValue;
        }else {
      // if(hiddenValue === "Siding" || hiddenValue === "Roofing" || hiddenValue === "Windows" || hiddenValue === "Doors" || hiddenValue === "Gutters" ) {
      //    holderValue = hiddenValue;
      // }else {
         holderValue = $(this).text();
      }
        var placeholder = holderValue;

      $current.prepend($('<span>', {
        class: $current.attr('class').replace(/dyn/g, 'dyn__placeholder'),
        text: placeholder,
        'data-placeholder': placeholder
      }));
      return;
    }
    $current.children('div').append($('<span>', {
      class: $current.attr('class').replace(/dyn/g, 'dyn__box__options'),
      text: $(this).text()
    }));



  });
});



// Toggling the `.active` state on the `.sel`.
$('.dyn').click(function() {
  $(this).toggleClass('active');
});


// Toggling the `.selected` state on the options.
$('.dyn__box__options').click(function() {
  var txt = $(this).text();
  var index = $(this).index();

  $(this).siblings('.dyn__box__options').removeClass('selected');
  $(this).addClass('selected');

  var $currentDyn = $(this).closest('.dyn');
  $currentDyn.children('.dyn__placeholder').text(txt);
  $currentDyn.children('select').prop('selectedIndex', index + 1);
});


$(".modal-fullscreen").on('show.bs.modal', function () {
  setTimeout( function() {
  $(".modal-backdrop").addClass("modal-backdrop-fullscreen");

  }, 0);
  });
  $(".modal-fullscreen").on('hidden.bs.modal', function () {
  $(".modal-backdrop").addClass("modal-backdrop-fullscreen");
  $("body").css("padding-right","0");
  console.log("home-pading-test");
  });

});

// var $ = jQuery.noConflict(); // use value of hidden CF7 input to set selected option in select field $(document).ready(function(){ // get contents of hidden input; store in variable
//   var val =  $("input.wpcf7-dynamichidden").val(); // set the "selected" attribute for option with value equal to variable
//   console.log(val);
//   $('select#projectfields option[value=' + val + ']').attr('selected', 'selected');

<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package StrapPress
 */

?>

<div class="row-wrp logo-bar wow fadeInDown" data-wow-duration="2s" data-wow-delay="0.05s">

	<?php if( have_rows('logos', 'options') ): ?>

	    <ul id="footer-logos">

	    <?php while( have_rows('logos', 'options') ): the_row(); ?>

	        <li> <img src="<?php the_sub_field('logo', 'options');?>"/></li>

	    <?php endwhile; ?>

	    </ul>

	<?php endif; ?>

</div>
<div class="row-wrp footer-mid">

	<div class="ft-tag-wrp"><?php the_field('tagline', 'options'); ?></div>
	<div class="ft-soc-wrp">
	<?php the_field('social_text', 'options'); ?>
<i class="fa fa-twitter" aria-hidden="true"></i>  <i class="fa fa-facebook-official" aria-hidden="true"></i> <i class="fa fa-instagram" aria-hidden="true"></i>



	<!-- <?php the_field('social_media', 'options'); ?> -->
	</div>
	<div class="ft-cta-wrp">
	<?php the_field('cta', 'options'); ?>
	</div>
</div>
<div class="row-wrp footer-lwr">

		<div class="container-fluid">

		 <div class="navbar-brand mb-0"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php bloginfo('template_url')?>/images/footer-logo.png"/></a></div>
			<div class="footer-nav" id="navbarNav">
					<?php
					$args = array(
						'theme_location' => 'primary',
						'depth'      => 2,
						'container'  => false,
						'menu_class'     => 'footer-navbar-nav',
						'walker'     => new Bootstrap_Walker_Nav_Menu()
						);
					if (has_nav_menu('primary')) {
						wp_nav_menu($args);
					}
					?>
				</div>
				<div id="footer-contact">
				<div class="phone">
					<i class="fa fa-phone-square" aria-hidden="true"></i> <?php the_field('contact_number', 8); ?>
				</div>

					<a href="#" class="btn btn-outline-primary" data-toggle="modal" data-target="#modal-fullscreen">Free Quote</a>

			</div>


</div>

<?php wp_footer(); ?>
<script>



</script>
</body>
</html>

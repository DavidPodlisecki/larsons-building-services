<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package StrapPress
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
	<div id="wptime-plugin-preloader"></div>
<div id="page" class="site">

	<header id="masthead" class="site-header" role="banner">
	    <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
	    	<div class="container-fluid">

				<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				</button>
				 <div class="navbar-brand mb-0"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php bloginfo('template_url')?>/images/Larsons_building_services_logo.png"/></a></div>
		   		<div class="collapse navbar-collapse" id="navbarNav">
	            <?php
	            $args = array(
	              'theme_location' => 'primary',
	              'depth'      => 2,
	              'container'  => false,
	              'menu_class'     => 'navbar-nav',
	              'walker'     => new Bootstrap_Walker_Nav_Menu()
	              );
	            if (has_nav_menu('primary')) {
	              wp_nav_menu($args);
	            }
	            ?>
							<div id="header-contact" class="d-block d-md-block d-lg-none">
								<div class="phone">
									<i class="fa fa-phone-square" aria-hidden="true"></i><?php the_field('contact_number', 8); ?>
								</div>
								<a href="#" class="btn btn-outline-primary main-modal" data-toggle="modal" data-target="#modal-fullscreen">Free Quote</a>

								</div>
	          </div>
						<div id="header-contact" class="d-none d-md-none d-lg-block">
							<div class="phone">
								<i class="fa fa-phone-square" aria-hidden="true"></i><?php the_field('contact_number', 8); ?>
							</div>
							<a href="#" class="btn btn-outline-primary main-modal" data-toggle="modal" data-target="#modal-fullscreen">Free Quote</a>

							</div>

	        </div>
					<!-- Modal fullscreen -->
					<div class="modal modal-fullscreen fade" id="modal-fullscreen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<button type="button" class="close" data-dismiss="modal">
									<img src="<?php bloginfo('template_url')?>/images/close.png" alt="">
								</button>
								<div class="modal-inner">
									 <?php echo do_shortcode('[contact-form-7 id="379" title="Quote Form"]'); ?>
								</div>
							</div>
							<!-- <div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div> -->
						</div>
					</div>
		</nav>
	</header><!-- #masthead -->
	<div id="content" class="site-content">
		<!-- PAGE HERO -->
		<?php
		if (is_post_type_archive('testimonial')) {
			get_template_part('template-parts/hero', 'testimonial');
		} else {
			while ( have_posts() ) : the_post();
					if (is_page_template('home.php')) {
					// Default homepage
					get_template_part( 'template-parts/hero', 'home' );
				} elseif (is_page_template('services.php')) {
					get_template_part('template-parts/hero', 'services');
				} elseif (is_page_template('contact.php')) {
					get_template_part('template-parts/hero', 'contact');
				} elseif(is_singular( 'testimonial' ) ){
					get_template_part( 'template-parts/hero', 'page' );
			 }
				 else{
					//everything else
					get_template_part( 'template-parts/hero', 'page' );
				};
			endwhile; // End of the loop.
		};
		?>

		<?php wp_reset_postdata(); ?>


<!-- END PAGE HERO -->

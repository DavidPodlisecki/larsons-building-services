<script>
	jQuery(document).ready(function($) {
		$('.carousel').carousel({
			interval: false,
			pause: true
		});
	});
</script>
<!-- <div id="page-hero" class="hero parallax-window"  iosFix="false"  data-parallax="scroll"  data-image-src="<?php the_field('home_hero_background');?>">
<div class="inner">
		<div class="copy vertical">
			<h1><?php the_field('hero_heading')?></h1>
		</div>

		</div> -->
<div id="page-hero" class="row-wrp hero img-row parallax-window" iosFix="false"  data-parallax="scroll"  data-image-src="<?php the_field('home_hero_background');?>">
	<div class="parent-col left wow fadeInUp" data-wow-duration="2s">
		<div class="child-col right">
			<h1><?php the_field('hero_heading')?></h1>
		</div>
	</div>
	<div class="parent-col right">
		<?php
		$query = new WP_Query( array( 'post_type' => 'testimonial', 'posts_per_page' => 4 ) );
			if ( $query->have_posts() ) : ?>
			<?php $countlist = -1; ?>
			<?php $countpost = -1; ?>
			<div class="testimonials-slider">
				<div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel">
					<ol class="carousel-indicators">
						<?php while ( $query->have_posts() ) : $query->the_post(); ?>
							<?php $countlist++; ?>
								<li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $countlist; ?>" class="<?php if ($countlist == 0) { echo "active";}?>"></li>
					    <?php endwhile; ?>
					</ol>
					<div class="carousel-inner" role="listbox">
						<?php while ( $query->have_posts() ) : $query->the_post(); ?>
						<?php $countpost++; ?>
						<div class="carousel-item <?php echo $countpost; ?> <?php if ($countpost == 0) { echo "active";}?>">
							<div class="item-row">
								<?php the_excerpt(); ?>
							</div>
							<div class="item-row">
								<a href="<?php the_permalink(); ?>" class="btn btn-outline-primary">Read More</a>
							</div>
						</div>
						<?php endwhile;  wp_reset_postdata();?>
					</div>
				</div>
				<?php else : ?>
					<!-- show no testimonials in the database... add some -->
				<?php endif;  ?>
			</div>
		</div>



	</div>

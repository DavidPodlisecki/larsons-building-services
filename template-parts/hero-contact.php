<div id="page-hero" class="row-wrp hero img-row parallax-window" iosFix="false"  data-parallax="scroll"  data-image-src="<?php the_field('hero_bg');?>">
<!-- <div class="row-wrp img-row m0a" style=" background-image: url(<?php the_field('hero_bg');?>);"> -->
	<div class="parent-col left">
		<div class="child-col right">
			<h2><?php the_field('heading'); ?></h2>
			<h3><?php the_field('sub_heading'); ?></h3>
			<div class="hm-row-txt">
				<?php the_field('text'); ?>
				<a href="#" class="btn btn-outline-primary">Free Quote</a>
			</div>
		</div>
	</div>
	<div class="parent-col right">
			<div class="contact-info-wrp">
				<div class="name"><?php the_field('contact_name'); ?></div>
				<div class="phone"><i class="fa fa-phone-square" aria-hidden="true"></i> <?php the_field('contact_number'); ?></div>
				<div class="address"><?php the_field('address_line_1'); ?></div>
				<div class="address"><?php the_field('address_line_2'); ?></div>
			</div>
	</div>
</div>

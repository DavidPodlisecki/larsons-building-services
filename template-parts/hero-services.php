<?php
$values = get_field( 'services_hero_background' );
if ( $values ) {
    $img_src = $values;
} else {
    $img_src = get_field('services_hero_background', $post->post_parent );
}
?>


<!-- <div id="page-hero" class="hero parallax-window"  iosFix="false"  data-parallax="scroll"  data-image-src="<?php the_field('home_hero_background');?>"> -->

<div id="page-hero" class="hero service-hero parallax-window"  iosFix="false"  data-parallax="scroll"  data-image-src="<?php echo $img_src ?>">
<!-- <div id="page-hero" class="hero service-hero" style="background-image:url('<?php echo $img_src ?>');"> -->
	<div class="inner">
	 <div class="copy vertical">

	 </div>

 </div>
</div>
<div class="services_nav d-flex justify-content-between">
	<a class="servce-btn" href="<?php echo get_site_url(); ?>/services/siding" id="siding-btn">
		<span class="icon-siding"></span>
		Siding
	</a>
	<a class="servce-btn" href="<?php echo get_site_url(); ?>/services/roofing" id="roofing-btn">
		<span class="icon-roofing"></span>
		Roofing
	</a>
	<a class="servce-btn" href="<?php echo get_site_url(); ?>/services/windows" id="windows-btn">
		<span class="icon-windows"></span>
		Windows
	</a>
	<a class="servce-btn" href="<?php echo get_site_url(); ?>/services/gutters" id="gutters-btn">
		<span class="icon-gutters"></span>
		Gutters
	</a>
	<a class="servce-btn" href="<?php echo get_site_url(); ?>/services/doors" id="doors-btn">
		<span class="icon-door"></span>
		Doors
	</a>
</div>

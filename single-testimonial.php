<?php
/**
 * The template for displaying all services
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package StrapPress
 */

get_header(); ?>


<div id="col-primary" class="col-content-area">
	<main id="col-main" class="col-site-main" role="main">

					<?php
					while ( have_posts() ) : the_post();

?>
<div class="row-wrp ">
	<div class="parent-col m1200">
		<div class="child-col left">
			<h2><?php the_title(); ?></h2>
			<h3><?php the_field('services'); ?></h3>
		</div>
	</div>
</div>
<div class="split-wrp">
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content split-col">

		<?php
			the_content();
		?>

		<?php
$prev_post = get_previous_post();
if($prev_post) {
 $prev_title = strip_tags(str_replace('"', '', $prev_post->post_title));
 echo "\t" . '<a rel="prev" href="' . get_permalink($prev_post->ID) . '" title="' . $prev_title. '" class=" ">&laquo; Previous Testimonial</a>' . "\n" . "|" ."\n";
}

$next_post = get_next_post();
if($next_post) {
 $next_title = strip_tags(str_replace('"', '', $next_post->post_title));
 echo "\t " . '<a rel="next" href="' . get_permalink($next_post->ID) . '" title="' . $next_title. '" class=" ">Next Testimonial &raquo;</a>' . "\n";
}
?>

	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
				edit_post_link(
					sprintf(
						/* translators: %s: Name of current post */
						esc_html__( 'Edit %s', 'strappress' ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					),
					'<span class="edit-link">',
					'</span>'
				);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-## -->

</div>

<!-- START GALLERY -->
<div class="row-wrp">
	<?php
	$images = get_field('gallery');
	if( $images ): ?>
		<div id="carouselControls" class="carousel slide" data-interval="false" data-pause="hover" data-ride="carousel">
			<div class="carousel-inner" role="listbox">
				<?php
				$counter = 1;
	      $slideto = -1;
				foreach( $images as $image ): $slideto++ ?>
				<?php if ($counter%4 == 1) {
					if ($counter == 1) {
						echo '<div class="carousel-item active">';
					}else{
						echo '<div class="carousel-item">';
					}
					}
				?>
					<div class="list-inline-item thumnav <?php if ($counter == 1) {echo 'active';} ?>">
						<?php $counter++; ?>
						<a id="carousel-selector-<?php echo($slideto); ?>" class="selected" data-slide-to="<?php echo($slideto); ?>" data-target="#myCarousel">
								<img src="<?php echo $image['sizes']['testimonial-thumb']; ?>" alt="<?php echo $image['alt']; ?>" class="img-fluid" />
						</a>
					</div>
				<?php if ($counter%4 == 1) {echo '</div>';} ?>
				<?php endforeach; ?>
			</div>
			<a class="carousel-control-prev" style="width:5%;"href="#carouselControls" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" style="width:5%;" href="#carouselControls" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
		<div class="col-lg-12" id="slider">
			<div id="myCarousel" class="carousel slide" data-interval="false" data-pause="hover">
				<!-- main slider carousel items -->
				<div class="carousel-inner" style="max-width:1075px; margin:0 auto;">
						<?php
							$slidecount = -1;
							foreach( $images as $image ):
							$slidecount++; ?>
						<div class="<?php if ($slidecount == 0) {echo 'active';} ?> item carousel-item" data-slide-number="<?php echo($slidecount); ?>">
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="img-fluid">
						</div>
						<?php endforeach; ?>
						<!-- <a class="carousel-control left pt-3 vertical" href="#myCarousel" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
						<a class="carousel-control right pt-3 vertical"	 href="#myCarousel" data-slide="next"><i class="fa fa-chevron-right"></i></a> -->

						<a class="carousel-control-prev"href="#myCarousel" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon blue" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
							<span class="carousel-control-next-icon blue" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>



				</div>
								<!-- main slider carousel nav controls -->
		</div>
		<?php endif; ?>
		<!-- End if images-->
</div>

<script>
jQuery(document).ready( function($){

	$('.carousel').carousel({
	interval: false
});
$(".thumnav").click(function () {
    $(".thumnav").removeClass("active");
    $(this).addClass("active");
});

 });
 </script>

<!-- END GALLERY -->

<?php
					endwhile; // End of the loop.
					?>
					<?php query_posts( array(
						 'post_type' => 'testimonial',
						 'posts_per_page' => 6

					)); ?>
								<?php if (have_posts()) :?>
									<div class="row-wrp">
									<div class="parent-col left">
									<div class="child-col right">
									 <h2 class="testimonial_section_title">More Testimonials</h2>
								 </div>
							 </div>
						 </div>
						 <div class="row-wrp m0015">
								<?while ( have_posts() ) : the_post();
									 // echo $current_id;
									 ?>
					<div class="testimoanil-more-wrp">
						<a href="<?php the_permalink();?>" class="more-testimonial-thumb" style="background-image:url('<?php the_field('hero_bg');?>');">

					 	</a>
					</div>


					<?php
								endwhile; // End of the loop.
							endif;
								?>
								</div>
					<?php wp_reset_postdata(); ?>

	</main>
</div>


<?php
get_footer();
